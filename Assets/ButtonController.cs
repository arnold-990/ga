﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    public TextMeshProUGUI level;
    public Slider slider;
    public Button button;
    int value;

    public void ButtonClick()
    {
        slider.value += 0.1f;
        if (slider.value >= slider.maxValue)
        {
            slider.maxValue = slider.maxValue * 3;
            slider.value = 0;
            value++;
            level.text = "Level : " + value;
            GAManager.instance.Check(value);
        }
    }
}