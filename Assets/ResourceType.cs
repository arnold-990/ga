﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceType : MonoBehaviour
{
    public float Gold;
    public float House;
    public float People;

    public ResourceType instance;
    private void Awake()
    {
        instance = this;
    }
}