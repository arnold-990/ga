﻿using UnityEngine;
using UnityEngine.Advertisements;

public class AdService
{
    public string gameId = "1234";
    public bool testMode = true;

    public void Initialize()
    {
        Advertisement.Initialize(gameId, testMode);
    }
}